import axios from 'axios';
import { useEffect, useState } from 'react';
import './app.css'


function App() {
  const [temperature, setTemperature] = useState(0)
  const [stateLed, setStateLed] = useState("")
  const [stateTemp, setStateTemp] = useState("Capteur de température Démarré")
  const isLighting = stateLed === "Light" ?? "No Light"
  const isTemp = stateTemp === "Capteur de température Stoppé" ?? "Capteur de température Démarré"

  useEffect(() => {
    const getTemeprature = async () => {
      try {
        const reponse = await axios.get('http://192.168.78.224/temp')
        setTemperature(reponse.data)
      }
      catch (er) {
        console.log(er)
      }
    }
    getTemeprature()

  })

  const switchTemp = async () => {
    try {
      const response = await axios.get('http://192.168.78.224/stopstartTemp')
      setStateTemp(response.data)
    } catch (error) {
      console.log(error)
    }
  }

  const switchOnLed = async () => {
    try {
      const response = await axios.get('http://192.168.78.224/light')
      setStateLed(response.data)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <div className="App">
      <div className='title'>
        <h1>Capteur de temperature</h1>
      </div>
      <div className='content'>
        <p>Valeur du capteur:  <span>{temperature}</span></p>

        {isLighting ?
          <button onClick={switchOnLed}>Éteindre la led</button>
          :
          <button onClick={switchOnLed}>Allumer la led</button>
        }
        {isTemp ?
          <button onClick={switchTemp}>Allumer le capteur</button>
          :
          <button onClick={switchTemp}>Éteindre le capteur de température</button>
        }
      </div>


    </div>
  );
}

export default App;
