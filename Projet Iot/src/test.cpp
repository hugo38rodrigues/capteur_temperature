// #include <WiFi.h>
// #include <WebServer.h>

// const char* ssid = "VotreSSID";
// const char* password = "VotreMotDePasse";

// WebServer server(80);

// void handleRoot() {
//   server.send(200, "text/plain", "Bonjour du serveur ESP32 !");
// }

// void handleValeur() {
//   if (server.method() == HTTP_POST) {
//     String data = server.arg("plain");
//     Serial.print("Valeur reçue : ");
//     Serial.println(data);
//     server.send(200, "text/plain", "Valeur reçue : " + data);
//   } else {
//     server.send(405, "text/plain", "Méthode non autorisée");
//   }
// }

// void setup() {
//   Serial.begin(115200);

//   // Connexion au réseau WiFi
//   WiFi.begin(ssid, password);
//   while (WiFi.status() != WL_CONNECTED) {
//     delay(1000);
//     Serial.println("Connexion au WiFi en cours...");
//   }
//   Serial.println("Connecté au réseau WiFi");

//   // Route pour la page d'accueil
//   server.on("/", handleRoot);

//   // Route pour recevoir la valeur
//   server.on("/valeur", HTTP_POST, handleValeur);

//   // Démarrer le serveur
//   server.begin();
//   Serial.println("Serveur démarré");
// }

// void loop() {
//   server.handleClient();
// }
