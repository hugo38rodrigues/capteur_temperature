#include "Request.h"
#include <HTTPClient.h>

bool response(int httpCode){
    if (httpCode > 0) {
    if (httpCode == HTTP_CODE_OK) {
      return true;
    } else {
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);
      return false;
    }
  } else {
    return false;
  }
}

String Request::post(String url, String query){
    
    HTTPClient http;
    http.begin(url);
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    
    int httpCode = http.POST(query);
    if(response(httpCode)){
        String payload = http.getString();
        http.end();
        return payload;
    }else{
        return "Error";
    }
    // String payload = http.getString();
    // Serial.println(httpCode);
    // Serial.println(payload);
}


String Request::get(String url){
    
    HTTPClient http;
    http.begin(url);

    int httpCode = http.GET();
    if(response(httpCode)){
        String payload = http.getString();
        http.end();
        return payload;
    }else{
        return "Error";
    }
    
}

