#include <Arduino.h>

class Request{
    public :
        String post(String url, String query); // Use std::string instead of String
        String get(String url);
};
