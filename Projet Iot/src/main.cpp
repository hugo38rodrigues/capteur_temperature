#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "Temp.h"
#include "Request.h"
#include <WebServer.h>
#include <DallasTemperature.h>
#include <OneWire.h>

const char WIFI_SSID[] = "iPhone de Guilhem";
const char WIFI_PASSWORD[] = "Poulet26";

String HOST_NAME = "http://192.168.78.143:3000";
int led_pin = 19;
int temp_pin = 15;

Temp temp;
Request request;
WebServer server(80);

OneWire oneWire(temp_pin);
DallasTemperature sensors(&oneWire);

int count = 0;
bool light = false;
bool recup_temp = true;

void handleRoot() {
  server.send(200, "application/text", "Bonjour du serveur ESP32 !");
}
void handleLight() {
  if(light){
    digitalWrite(led_pin, HIGH);
    server.send(200, "application/json", "Light");
    light = false;
  }else{
    digitalWrite(led_pin, LOW);
    server.send(200, "application/json", "No Light");
    light = true;
  }
}

void handleTemp() {
  if(recup_temp){
    sensors.requestTemperatures(); // Send the command to get temperatures
    count++;
    sensors.setUserDataByIndex(0, count);
    int x = sensors.getUserDataByIndex(0);
    server.send(200, "application/json", String(sensors.getTempCByIndex(0)) + "°C");
  }else{
    server.send(200, "application/json", "Capteur de température à l'arrêt");
  
  }
  
}

void handleStopTemp(){
  if(recup_temp){
    recup_temp = false;
    server.send(200, "application/json", "Capteur de température Stoppé");
  }else{
    recup_temp = true;
    server.send(200, "application/json", "Capteur de température Démarré");

  }
}

void beginWifi(){
   WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("Connecting to WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi");
  Serial.print(WiFi.localIP());
}

void beginDallas(){
  Serial.println("Dallas Temperature Initialisation...");
  sensors.begin();
}

void setup(){
  Serial.begin(9600);
  temp.pin = temp_pin;
  pinMode(led_pin, OUTPUT);

  beginWifi();

  server.on("/", handleRoot);
  server.on("/light", handleLight);
  server.on("/temp", handleTemp);
  server.on("/stopstartTemp", handleStopTemp);
  server.begin();
  Serial.println("Server started");
}

void loop(){
  server.handleClient();
  delay(200);
}
